/*global module:false*/
module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    // Task configuration.
    jshint: {
      options: {
        curly: true,
        eqeqeq: true,
        immed: true,
        latedef: true,
        newcap: true,
        noarg: true,
        sub: true,
        undef: false,
        unused: true,
        boss: true,
        eqnull: true,
        browser: true,
        globals: {
          jQuery: true
        }
      },
      gruntfile: {
        src: 'Gruntfile.js'
      },
      lib_test: {
        src: ['lib/**/*.js', 'test/**/*.js']
      }
    },
    qunit: {
      files: ['test/**/*.html']
    },
    sass: {
      dist: {
        files: [{
          expand: true,
          cwd: 'stylesheets',
          src: ['*.sass'],
          dest: 'public',
          ext: '.css'
        }]
      }
    },
    connect: {
      server: {
        options: {
          port: 9000,
          hostname: '*',
          open: true,
          livereload: true
        }
      }
    },
    watch: {
      css: {
        files: 'stylesheets/**/*.sass',
        tasks: ['sass'],
        options: {
          livereload: 35729,
        },
      },
      html: {
        files: 'index.html',
        options: {
          livereload: 35729,
        },
      },
      gruntfile: {
        files: '<%= jshint.gruntfile.src %>',
        tasks: ['jshint:gruntfile']
      },
      lib_test: {
        files: '<%= jshint.lib_test.src %>',
        tasks: ['jshint:lib_test', 'qunit']
      }
    }
  });

  // These plugins provide necessary tasks.
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-qunit');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');

  // Default task.
  grunt.registerTask('default', ['jshint', 'qunit', 'sass', 'connect', 'watch']);

};
